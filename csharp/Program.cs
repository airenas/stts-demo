﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Web;

namespace CSharpDemo
{
    class Program
    {
        const String url = @"http://78.62.69.51:8080/stts/model/decoder?model={0}&key={1}";
        const String modelis = "skaiciai";
        const String raktas = "s1";

        // Atpažįsta garso įrašą
        public String DoDecode(string file)
        {

            Console.Out.WriteLine("Failas\t\t {0}", file);
            Console.Out.WriteLine("Modelis\t\t {0}", modelis);
            Console.Out.WriteLine("Raktas\t\t {0}", raktas);
            String decodeurl = String.Format(url, modelis, raktas);
            Console.Out.WriteLine("Url\t\t {0}", decodeurl);
            HttpWebRequest req = WebRequest.Create(new Uri(decodeurl)) as HttpWebRequest;
            req.Method = "POST";
            req.ContentType = "audio/x-wav";
            Console.Out.WriteLine("\nSkaitome failą ....");
            byte[] wavData = File.ReadAllBytes(file);

            req.ContentLength = wavData.Length;

            Console.Out.WriteLine("Siunčiame failą ....");
            using (Stream post = req.GetRequestStream())
            {
                post.Write(wavData, 0, wavData.Length);
            }

            Console.Out.WriteLine("Skaitomę atsakymą ....");
            string result = null;
            using (HttpWebResponse resp = req.GetResponse()
                                          as HttpWebResponse)
            {
                StreamReader reader =
                    new StreamReader(resp.GetResponseStream());
                result = reader.ReadToEnd();
            }
            return result;
        }

        // main metodas
        static void Main(string[] args)
        {
            Console.Out.WriteLine("Stts demonstracinė programa.");
            if (args == null || args.Length < 1)
            {
                Console.Error.WriteLine("Nurodykite failą.");
            }
            else
            {
                String file = args[0];
                try
                {
                    String result = new Program().DoDecode(file);
                    Console.Out.WriteLine("\nAtsakymas:\t\t {0}\n", result);
                }
                catch (Exception e)
                {
                    Console.Error.WriteLine("Klaida\n");
                    Console.Error.WriteLine(e);
                }
            }
            Console.Out.WriteLine("Spauskite ENTER");
            Console.In.ReadLine();
        }
    }
}
