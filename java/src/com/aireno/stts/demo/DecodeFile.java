package com.aireno.stts.demo;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

public class DecodeFile {
    final String FULL_URL = "http://78.62.69.51:8080/stts/model/decoder?model=%s&key=%s";
    //final String FULL_URL = "http://192.168.1.59:8080/stts/model/decoder?model=%s&key=%s";
    final String MODELIS = "skaiciai";
    final String RAKTAS = "s1";

    public static void main(String[] args) throws IOException {
        System.out.println("Stts demonstracinė programa.\n");
        if (args == null || args.length < 1) {
            System.out.println("Nurodykite failą.\n");
        } else {
            String file = args[0];
            try {
                String result = new DecodeFile().doDecode(file);
                System.out.printf("\nAtsakymas:\t\t %s\n\n", result);
            } catch (Exception e) {
                System.err.println("\nKlaida\n");
                e.printStackTrace();
            }
        }
        System.out.println("\nSpauskite ENTER\n");
        System.in.read();

    }

    private String doDecode(String file) throws Exception {
        System.out.printf("Failas\t\t %s\n", file);
        System.out.printf("Modelis\t\t %s\n", MODELIS);
        String url = String.format(FULL_URL, URLEncoder.encode(MODELIS, "UTF-8"), URLEncoder.encode(RAKTAS, "UTF-8"));
        System.out.printf("Url\t\t %s\n", url);

        URLConnection urlConn = new URL(url).openConnection();
        urlConn.setDoOutput(true);
        urlConn.setUseCaches(false);
        urlConn.setRequestProperty("Content-Type", "audio/x-wav");

        System.out.printf("\nSkaitome failą ....\n");


        File fileWav = new File(file);
        byte[] wavData = new byte[(int) fileWav.length()];
        try (DataInputStream dis = new DataInputStream(new FileInputStream(fileWav))) {
            dis.readFully(wavData);
        }

        System.out.printf("Siunčiame failą ....\n");
        try (OutputStream outputStream = urlConn.getOutputStream()) {
            outputStream.write(wavData, 0, wavData.length);
        }

        System.out.printf("Skaitomę atsakymą ....\n");
        StringBuilder result = new StringBuilder();
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(urlConn.getInputStream(), StandardCharsets.UTF_8))) {
            String line;
            while ((line = br.readLine()) != null) {
                result.append(line);
            }
        }
        return result.toString();
    }
}

