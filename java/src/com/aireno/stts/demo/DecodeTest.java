package com.aireno.stts.demo;

import com.aireno.audio.decoder.DecoderManager;
import com.aireno.audio.decoder.events.DecoderEvent;
import com.aireno.audio.decoder.events.DecoderEventListener;
import com.aireno.audio.decoder.events.DecoderEventParam;
import org.apache.commons.lang.StringUtils;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.text.spi.DateFormatProvider;
import java.util.Date;

public class DecodeTest {
    final static String URL = "http://78.62.69.51:8080/stts/model/decoder";
    //final static String URL = "http://192.168.1.59:8080/stts/model/decoder";
    final static String MODELIS = "testKomandos";
    final static String RAKTAS = "k1";

    // galimos frazės
    final static String S_BAIGTI = "baigti";
    final static String S_LAIKAS = "kiek dabar laiko";
    final static String S_GOOGLE = "atidaryk gūglą";

    public static void main(String[] args) throws IOException {
        System.out.println("Stts demonstracinė programa.\n");

        try {
            System.out.println("Frazės:\n");
            System.out.printf(" - %s\n", S_BAIGTI);
            System.out.printf(" - %s\n", S_LAIKAS);
            System.out.printf(" - %s\n", S_GOOGLE);

            new DecodeTest().doDecode();

        } catch (Exception e) {
            System.err.println("Klaida\n");
            e.printStackTrace();
            System.out.println("\nSpauskite ENTER\n");
            System.in.read();
        }
    }

    private void doDecode() throws Exception {
        DecoderManager decoderManager = new DecoderManager();

        // nustatome decoder manager'į
        initSettings(decoderManager);
        // prisirišame prie atpažinimo įvykių
        initListener(decoderManager);

        // pakviečiame inicializacijos metodą
        decoderManager.init();

        // klausome ir atpažįstame
        decoderManager.startListen();

        System.out.println("\nSpauskite ENTER, kad sustabdyti\n");
        // laukiame
        System.in.read();
        decoderManager.stopListen();
    }

    private void initSettings(DecoderManager decoderManager) {
        decoderManager.getSettings().setUrl(URL);
        decoderManager.getSettings().setModel(MODELIS);
        decoderManager.getSettings().setKey(RAKTAS);
    }

    private void initListener(final DecoderManager decoderManager) {
        decoderManager.addDecoderEventListener(new DecoderEventListener() {
            @Override
            public void handle(DecoderEventParam decoderEventParam) throws Exception {
                if (decoderEventParam.event == DecoderEvent.StartedDecoding) {   // siunčia į serverį
                    System.out.printf("\nLaukiam");
                }
                if (decoderEventParam.event == DecoderEvent.StartedRecording) { // pradėjo įrašinėti
                    System.out.printf("\nKlausau");
                }
                if (decoderEventParam.event == DecoderEvent.DecodeError) {      // klaida
                    System.out.printf("\nKlaida\n%s", decoderEventParam.result.getError());
                }
                if (decoderEventParam.event == DecoderEvent.Decoded) {
                    if (StringUtils.isNotEmpty(decoderEventParam.result.getError())) { // klaida
                        System.out.printf("\nKlaida\n%s", decoderEventParam.result.getError());

                    } else {
                        String result = decoderEventParam.result.getText();      // atpažinimo rezultatas
                        System.out.printf("\nGauta: %s", result);
                        processResult(result, decoderManager);                   // pagrindinis metodas valdymui :)
                    }
                }
            }
        });
    }

    private void processResult(String result, DecoderManager decoderManager) throws IOException, URISyntaxException {
        if (result == null) {
            return;
        }

        while (result.length() > 0) {
            if (result.startsWith(S_BAIGTI)) {
                System.out.printf("\nKOMPIUTERIS: Išeiname\n");
                decoderManager.stopListen();
                System.exit(1);
            } else if (result.startsWith(S_LAIKAS)) {
                result = result.substring(S_LAIKAS.length()).trim();
                System.out.printf("\nKOMPIUTERIS: Dabar yra %s\n", new SimpleDateFormat("HH:mm:ss").format(new Date()));
            } else if (result.startsWith(S_GOOGLE)) {
                result = result.substring(S_GOOGLE.length()).trim();
                System.out.printf("\nKOMPIUTERIS: Bandau atidaryti\n");
                Desktop.getDesktop().browse(new URI("www.google.lt"));
            } else {
                System.out.printf("\nKOMPIUTERIS: Nežinau ką daryti su %s\n", result);
                result = "";
            }
        }
    }
}

